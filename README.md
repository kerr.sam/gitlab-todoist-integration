# Overview

This project will sync the to-do item's from a GitLab account to a Todoist account. At this time, the sync is one-way from GitLab to Todoist.

Once setup, every time a to-do is added in GitLab, a corresponding to-do will be created in Todoist. The Todoist entry will
have the same name as the Issue or MR as in GitLab. The entry will also have a link to take you directly to the relevant
Gitlab Issue or MR.

## Setup

To run this, you should create a file called `.env` in the same directory as the Python script.
Inside this file, add the following:

```
GITLAB_API_TOKEN=<Your Gitlab API Token>
TODOIST_API_TOKEN=<Your Todoist API Token>
```

## Running

Once you've created the `.env` file, run these commands to run the sync:

- `python -m venv venv`
- `source venvsource venv/bin/activate`
- `pip install -r requirements.txt`
- `python doit.py`

### Recommended running configuration

Consider setting up a GitLab [scheduled pipeline](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) to run this script periodically to keep
GitLab and Todoist always in sync. You can run a scheduled pipeline every 30 minutes with `*/30 * * * *` as the Cron string for the
schedule.
